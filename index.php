<?php
/**
* Plugin Name: Plugin Name Here
* Plugin URI: www.pluginurlhere.com
* Description: Plugin Description Here
* Version: 1.0
* Author: Author Name Here
* Author URI: www.authorsiteurlhere.com
*/
/*  
The MIT License (MIT)
Copyright (c) [2014] [Mark Alexander]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

//add action to set up the admin menu entry
add_action('admin_menu', 'admin_menu_function');
 
function admin_menu_function(){
        //add options page
        //synatx: add_options_page( $page_title, $menu_title, $capability, $menu_slug, $page_function, $icon_url, $position );
        add_options_page( 'Plugin Page Title', 'Plugin Menu Title', 'manage_options', 'slug', 'page_function' );

        //or add admin menu
        //syntax: add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $page_function, $icon_url, $position );
        add_menu_page( 'Plugin Page Title', 'Plugin Menu Title', 'manage_options', 'slug', 'page_function' );
}

function page_function(){
    //include the admin page template
    include __DIR__."/options.php";
}
 
function addjs(){
    // Register the script like this for a plugin:
    wp_register_script( 'main', plugins_url( '/js/main.js', __FILE__ ) , array( 'jquery' ) );
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'main' );
    $wnm_custom = array( 'plugin_url' => plugins_url() );
    wp_localize_script( 'plugin_folder_name', 'wnm_custom', $wnm_custom );
}

//add action for adding js function
add_action( 'wp_enqueue_scripts', 'mainjs' );

function add_styles()
{
    // Register the style like this for a plugin:
    wp_register_style( 'name_of_style', plugins_url( '/css/styles.css', __FILE__ ), array(), '20120208', 'all' );
    // For either a plugin or a theme, you can then enqueue the style:
    wp_enqueue_style( 'name_of_style' );
}

//add action for adding styles function
add_action( 'wp_enqueue_scripts', 'add_styles' );

//tell wordpress to register the demolistposts shortcode
add_shortcode("shortcode_to_use", "plugin_handler");

function plugin_handler() {
    //run function that actually does the work of the plugin
    $output = plugin_function();
    //send back text to replace shortcode in post
    return $output;
}

function plugin_function() {
    //process plugin
    include('plugin.php');
    //send back text to calling function
    return $output;
}
?>